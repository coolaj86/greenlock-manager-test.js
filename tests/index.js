"use strict";

var Tester = require("../");

var Manager = require("greenlock-manager-fs");
var config = {
    configFile: "greenlock-manager-test.delete-me.json"
};

Tester.test(Manager, config)
    .then(function() {
        console.log("PASS: Known-good test module passes");
    })
    .catch(function(err) {
        console.error("Oops, you broke it. Here are the details:");
        console.error(err.stack);
        console.error();
        console.error("That's all I know.");
    });

#!/usr/bin/env node
"use strict";

var fs = require("fs");
var path = require("path");

function tmpl() {
    var src = path.join(__dirname, "tmpl/manager.tmpl.js");
    var dst = path.join(process.cwd(), "./manager.js");

    try {
        fs.accessSync(dst);
        console.warn("skip  'manager.js': already exists");
        return;
    } catch (e) {
        fs.writeFileSync(dst, fs.readFileSync(src, "utf8"), "utf8");
        console.info("wrote 'manager.js'");
    }
}

function tmplTest() {
    var srcTest = path.join(__dirname, "tmpl/manager.test.tmpl.js");
    var dstTest = path.join(process.cwd(), "./manager.test.js");

    try {
        fs.accessSync(dstTest);
        console.warn("skip  'manager.test.js': already exists");
        return;
    } catch (e) {
        fs.writeFileSync(dstTest, fs.readFileSync(srcTest, "utf8"), "utf8");
        console.info("wrote 'manager.test.js'");
    }
}

tmpl();
tmplTest();
